#!/usr/bin/env bash

set -e
set -x

CI_PAGES_DOMAIN=${1}
CI_PAGES_URL=${2}
CI_PROJECT_TITLE=${3}
CI_PROJECT_URL=${4}
COMMIT_TIME=${5}
GITLAB_USER_NAME=${6}
GITLAB_USER_EMAIL=${7}
CI_COMMIT_SHA=${8}
CI_PROJECT_VISIBILITY=${9}

if ! type -p xelatex >/dev/null ; then
  >&2 echo "Please install xelatex" >&2
  exit 127
fi

if ! type -p pandoc >/dev/null ; then
  >&2 echo "Please install pandoc" >&2
  exit 127
fi

if ! type -p gs >/dev/null ; then
  >&2 echo "Please install ghostscript" >&2
  exit 127
fi

if ! type -p convert >/dev/null ; then
  >&2 echo "Please install imagemagick" >&2
  exit 127
fi

if ! type -p libreoffice >/dev/null ; then
  >&2 echo "Please install libreoffice" >&2
  exit 127
fi

if ! type -p rsync >/dev/null ; then
  >&2 echo "Please install rsync" >&2
  exit 127
fi

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
share_dir=${script_dir}/../share

mkdir -p ${dist_dir}

function makePage1 () {
  pdf_file=${1}
  pdf_basename=$(basename -- "${pdf_file}")
  pdf_dirname=$(dirname -- "${pdf_file}")
  pdf_filename="${pdf_basename%.*}"

  page1_pdf_dir=${pdf_dirname}
  page1_pdf="${page1_pdf_dir}/${pdf_filename}_page1.pdf"

  mkdir -p "${page1_pdf_dir}"

  gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${pdf_file}

  page1_pdf_png="${page1_pdf}.png"
  convert ${page1_pdf} ${page1_pdf_png}

  for size in 600 150
  do
    page1_pdf_png_size="${page1_pdf}-${size}.png"
    convert ${page1_pdf_png} -resize ${size}x${size} ${page1_pdf_png_size}
  done
}

# pre-process files
pre_src_files=$(find -L ${src_dir} -maxdepth 1 -type f | sort)

for pre_src_file in ${pre_src_files}; do
  pre_src_file_basename=$(basename -- "${pre_src_file}")
  pre_src_file_extension="${pre_src_file##*.}"
  if [ ${pre_src_file_extension} = "fodt" ] || \
     [ ${pre_src_file_extension} = "fodg" ] || \
     [ ${pre_src_file_extension} = "txt"  ] || \
     [ ${pre_src_file_extension} = "html" ]; then
    sed -e "s#\${CI_PAGES_DOMAIN}#${CI_PAGES_DOMAIN}#g" \
        -e "s#\${CI_PAGES_URL}#${CI_PAGES_URL}#g" \
        -e "s#\${CI_PROJECT_TITLE}#${CI_PROJECT_TITLE}#g" \
        -e "s#\${CI_PROJECT_URL}#${CI_PROJECT_URL}#g" \
        -e "s#\${COMMIT_TIME}#${COMMIT_TIME}#g" \
        -e "s#\${GITLAB_USER_NAME}#${GITLAB_USER_NAME}#g" \
        -e "s#\${GITLAB_USER_EMAIL}#${GITLAB_USER_EMAIL}#g" \
        -e "s#\${CI_COMMIT_SHA}#${CI_COMMIT_SHA}#g" \
        -e "s#\${CI_PROJECT_VISIBILITY}#${CI_PROJECT_VISIBILITY}#g" \
        ${pre_src_file} > ${dist_dir}/${pre_src_file_basename}
  else if [ ${pre_src_file_extension} = "md"   ]; then
     (echo -e "# ${CI_PROJECT_TITLE}" && \
     echo -e "\n----\n" && \
     cat ${pre_src_file} && \
     echo -e "\n----\n" && \
     echo -e "### This document\n\n" && \
     echo -e "* hosted at **[${CI_PAGES_URL}](${CI_PAGES_URL})**\n" && \
     echo -e "* source hosted at **[${CI_PROJECT_URL}](${CI_PROJECT_URL})**\n" && \
     echo -e "* last updated at time **${COMMIT_TIME}**\n" && \
     echo -e "* last updated by user **[${GITLAB_USER_NAME}](mailto:${GITLAB_USER_EMAIL})**\n" && \
     echo -e "* revision **[${CI_COMMIT_SHA}](${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA})**\n" && \
     echo -e "* access control **${CI_PROJECT_VISIBILITY}**\n" && \
     echo -e "\n----\n" && \
     cat ${share_dir}/licence.md &&\
     echo -e "\n----\n"
    ) > ${dist_dir}/${pre_src_file_basename}
  else
    rsync -aH ${pre_src_file} ${dist_dir}/${pre_src_file_basename}
  fi
  fi
done

# libreoffice
libre_src_files=$(find -L ${dist_dir} -maxdepth 1 -name '*.fodg' -o -name '*.fodt' -o -name '*.odg' -o -name '*.odt' -type f | sort)

for libre_src_file in ${libre_src_files}; do
  for format in pdf html; do
    libreoffice --invisible --headless --convert-to ${format} ${libre_src_file} --outdir ${dist_dir}
  done
done

# markdown
md_files=$(find -L ${dist_dir} -maxdepth 1 -name '*.md' -type f | sort)

for md_file in ${md_files}; do
  md_file_basename=$(basename -- "${md_file}")
  md_file_filename="${md_file_basename%.*}"

  for format in pdf html docx odt; do
    pandoc -fmarkdown-implicit_figures -M mainfont="DejaVu Sans Mono" --pdf-engine=xelatex ${md_file} -o ${dist_dir}/${md_file_filename}.${format}
  done
done

rsync -aH ${share_dir}/ ${dist_dir}

# page 1
pdf_files=$(find -L ${dist_dir} -maxdepth 1 -name '*.pdf' -type f | sort)

for pdf_file in ${pdf_files}; do
  pdf_file_basename=$(basename -- "${pdf_file}")
  pdf_file_filename="${pdf_file_basename%.*}"

  makePage1 ${dist_dir}/${pdf_file_filename}.pdf
done
